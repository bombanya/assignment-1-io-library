section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
	mov rax, 60
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
.count:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .count
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
	call string_length
	pop rdi
	mov rdx, rax
	mov rsi, rdi
	mov rax, 1
	mov rdi, 1
	syscall
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
	
; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, 1
	mov rdi, 1
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rdi
	ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push r12
	mov r12, rsp
	mov rax, rdi
	mov r9, 10
	push byte 0
	
.division:
	xor rdx, rdx
	div r9
	add rdx, '0'
	dec rsp
	mov [rsp], dl
	test rax, rax
	je .print
	jmp .division
	
.print:
	mov rdi, rsp
	call print_string
	mov rsp, r12
	pop r12
	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
	cmp rdi, 0
	jge print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals: ;rdi rsi
	mov rcx, -1
	xor rax, rax
	xor rdx, rdx
	
.loop:
	inc rcx
	mov al, [rdi + rcx]
	mov dl, [rsi + rcx]
	cmp al, dl
	jne .false
	
	test al, al
	je .true
	
	jmp .loop
	
.true:
	mov rax, 1
	ret
	
.false:
	mov rax, 0
	ret


; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	sub rsp, 1
	mov rax, 0
	mov rdi, 0
	mov rsi, rsp
	mov rdx, 1
	syscall
	test rax, rax
	je .end
	xor rax, rax
	mov al, byte [rsp]

.end:
	add rsp, 1
	ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word: ;rdi rsi
	push r12
	push r13
	push r14
	push r15
	
	test rsi, rsi
	je .error
	
	mov r12, rdi
	mov r13, rdi
	mov r14, rsi
	xor r15, r15

.spaces:
	call read_char
	cmp rax, 0x20
	je .spaces
	cmp rax, 0x9
	je .spaces
	cmp rax, 0xA
	je .spaces
	test rax, rax
	je .success
	
	jmp .word

.word:
	inc r15
	cmp r14, r15
	je .error
	mov [r13], al
	inc r13
	
	call read_char
	cmp rax, 0x20
	je .success
	cmp rax, 0x9
	je .success
	cmp rax, 0xA
	je .success
	test rax, rax
	je .success
	
	jmp .word
	
.success:
	mov [r13], byte 0
	mov rax, r12
	mov rdx, r15
	jmp .end
	
.error:
	xor rax, rax

.end:
	pop r15
	pop r14
	pop r13
	pop r12
	ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
	xor rax, rax
	xor rcx, rcx
	mov rdx, -1
	
.parse:
	inc rdx
	mov cl, [rdi + rdx]
	cmp cl, '0'
	jb .end
	
	cmp cl, '9'
	ja .end
	
	sub cl, '0'
	imul rax, rax, 10
	add rax, rcx
	
	jmp .parse

.end:
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
	push r12
	xor r12, r12
	mov r12b, [rdi]
	cmp r12b, '-'
	je .signed
	cmp r12b, '+'
	je .signed
	call parse_uint
	pop r12
	ret

.signed:
	inc rdi
	call parse_uint
	test rdx, rdx
	je .end
	inc rdx
	cmp r12b, '+'
	je .end
	neg rax
	
.end:
	pop r12
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy: ;rdi rsi rdx
	xor rcx, rcx
	
.loop:
	cmp rcx, rdx
	je .error
	mov al, [rdi + rcx]
	mov [rsi], al
	test al, al
	je .end
	inc rcx
	inc rsi
	jmp .loop
	
.error:
	xor rax, rax
	ret

.end:
	mov rax, rcx
	ret
